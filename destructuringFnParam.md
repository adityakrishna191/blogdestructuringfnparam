# Destructuring in Function Parameter

When writing a **function** that uses **object** as a parameter, we may only need specific **properties** and **not the entire object**, in those cases, we may use destructuring to only pass only the required properties to the function.

```js
const movieData = {
    filmName: "Matrix",
    releaseYear: 1999,
    genre: "Sci-Fi",
    leadCharacters: ["Trinity", "Neo", "Morpheus"],
}

//Before destructuring
const releaseInfoB4Destr = (obj) => {
    console.log( `${obj.filmName} was released in ${obj.releaseYear}` )
}

console.log(releaseInfoB4Destr(movieData)); //-> Matrix was released in 1999

//After destructuring
const releaseInfoAfterDestr = ({filmName, releaseYear}) => {
    console.log( `${filmName} was released in ${releaseYear}`)
}

console.log(releaseInfoAfterDestr(movieData)); //-> Matrix was released in 1999
```

Without destructuring we pass the whole `movieData` object into the function `releaseInfoB4Destr` and also our code has more characters, but with destructuring we are able to use only those properties `movieName` and `releaseYear`, that we need for our function `releaseInfoAfterDestr` and our code is concise.

When working with an **array** we can destructure the parameters and use them as an input in our function. It's useful when the number of arguments being passed to our function could be **dynamic**, with no fixed number. That way we won't have to worry about assigning parameters in our function declaration.

```js
const displayArguments = (...listOfArguments) => {
    listOfArguments.forEach((arg, index) => {
        console.log (`Argument number ${index+1} is: ${arg}`);
    })
}

console.log(displayArguments("a",1,"b",2,"c",3)); 
//->
// Argument number 1 is: a
// Argument number 2 is: 1
// Argument number 3 is: b
// Argument number 4 is: 2
// Argument number 5 is: c
// Argument number 6 is: 3
```

What we have done in the above example is use the rest operator (`...`) to gather all our arguments that we passed into the function `displayArguments` and use that inside to `console.log` the output.

---
References
- [Article on object destructuring](https://www.freecodecamp.org/news/javascript-object-destructuring-spread-operator-rest-parameter/)
- [Article on extracting function arguments](https://www.samanthaming.com/tidbits/20-destructuring-function-arguments/)
